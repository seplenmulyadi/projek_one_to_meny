<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Provinsi</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  
    <div class="body">
    <div class="container">
    <h1 class='text-center mb-4'>Categories</h1>
    
        <a href="{{route('category.add')}}"class="btn btn-primary">Tambah Kategories</a> 
        <table class="table table-striped">
            @csrf
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Kode Category</th>
                    <th scope="col">Nama Category</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>

            <tbody>

            @foreach($category as $row)
                <tr>
                    <td>{{$loop->iteration }}</td>
                    <td>{{$row->code_category}}</td>
                    <td>{{$row->name_category}}</td>
                    <td>
                        <a href= "{{route('category.edit',$row->id)}}"  title="Edit" class="btn btn-outline-warning"><i class="fa fa-edit"></i>Edit</a>
                        <a onclick = "return confirm ('Hapus Data?')" href="#"  title="Hapus" class="btn btn-outline-danger"><i class="fa fa-trash"></i>Delete</a>
                    </td>
                </tr>


            @endforeach
            </tbody>
        </table>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </body>
</html>
