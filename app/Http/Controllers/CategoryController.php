<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
     
        $category = Category ::all();
        return view('category.list', compact('category'));
    }

//-------------tamabah-----------------------------
    public function add()
    {
        return view('category.add');
    }

    public function store(Request $request){

        $category = new Category();
        $category->code_prov = $request->code_prov;
        $category->name_prov = $request->name_prov;
        try {
            $category->save();
            return redirect('category')->with('flash_message', 'Data Berhasil Ditambahkan!'); 
        }
        catch(\Exception $e)
        {
            return redirect('category')->with('flash_message', 'Data Berhasil Ditambahkan!'); 
        }
    }




//-------------Bagian Edit---------------------------------
    public function edit($id){
        $category = Category::FindOrFail($id);
        return view('category.edit', compact('category'));
    }

    public function update(Request $request ,$id){
        $category= Category ::findOrFail($id);
        $category->code_category =$request->code_category;
        $category->name_category =$request->name_category;

        try {
            $category->save($request->all());
            return redirect('category'); 
        }
        catch(\Exception $e)
        {
            return redirect('category'); 
        }
    }


}
