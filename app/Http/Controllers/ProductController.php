<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;


class ProductController extends Controller
{
    public function index()
    {    
    
        $product = Product::select("products.*",'categories.name_category as product_name')
            -> join('categories', 'categories.id', '=', 'products.code_category')
            ->orderBy('products.id')
            ->paginate(10);
        return view('products.list', compact('product'));
    }
}

