<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'code_category' => '12345',
            'name_category' => 'Elektronik',
        ]);
        DB::table('categories')->insert([
            'code_category' => '12346',
            'name_category' => 'Perabotan',
        ]);
        DB::table('categories')->insert([
            'code_category' => '12347',
            'name_category' => 'Pakaian',
        ]);
        DB::table('products')->insert([
            'code_product' => '12347',
            'name_product' => 'Pakaian',
            'code_category'=> '1',
            'price'=> "10000000"
        ]);
    }
}
