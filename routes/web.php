<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;

Route::get('/', function () {
    return view('welcome');
});

//Route::prefix('admin')->group(function(){

//Route::controller(App\Http\Controller\CategoryController::class)->group(function(){


    // kategori
    Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('index');

    Route::get('/category/add', [App\Http\Controllers\CategoryController::class, 'add'])->name('category.add');
    Route::post('/category/store', [App\Http\Controllers\CategoryController::class, 'store'])->name('category.store');


    Route::get('/category/edit/{id}', [App\Http\Controllers\CategoryController::class, 'edit'])->name('category.edit');
    Route::post('/category/update/{id}', [App\Http\Controllers\CategoryController::class, 'update'])->name('category.update');

    //produk
    Route::get('/products', [App\Http\Controllers\ProductController::class, 'index'])->name('index');